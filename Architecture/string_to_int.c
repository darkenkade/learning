#include <stdio.h>

int char_to_digit(char c) {return c-'0';}
int is_digit(char c) {return c>='0' && c<='9';}
int string_to_int(char* str){
  int n=0;
  if(*str){
    while(*str){
      if(!is_digit(*str)){
	return -1;
      }
      int digit=char_to_digit(*str);
      n*=10;
      n+=digit;
      ++str;
    }
  }
  else{
    return -1;
  }
  return n;
}

int main(int argc,char** argv){
  char* num=argv[1];
  printf("Input string %s\n",argv[1]);
  int ans = string_to_int(num);
  printf("Answer %d\n",ans);
}

