#include <stdio.h>

void insertion_sort(int s[],int n);
void print(int s[],int n);
void swap(int *a,int *b);
int main(){
  int arr[]={5,2,3,5,6,4,2,5,6,1,4,6,7};
  insertion_sort(arr,13);
  print(arr,13);
}

void swap(int* a,int* b){
  int tmp=*a;
  *a=*b;
  *b=tmp;
}

void insertion_sort(int s[],int n){
  for(int i=1;i<n;++i){
    int j=i;
    while((j>0) && (s[j]<s[j-1])){
      swap(&s[j],&s[j-1]);
      --j;
    }
  }
}

void print(int s[],int n){
  for(int i=0;i<n;++i){
    printf("%d ",s[i]);
  }
}

