#include <stdio.h>
#include <stdlib.h>

typedef struct{
  int size;
  char* items;
  int top;
} stack;

stack* make_stack(int s){
  stack* ss=(stack*)malloc(sizeof(stack*));
  ss->items=(char*)malloc(s*sizeof(char*));
  ss->size=s;
  ss->top=-1;
  return ss;
}

stack* push(stack* ss,char c){
  if(ss->top==ss->size-1){
    int size=ss->size;
    ss->items=realloc(ss->items,size*sizeof(ss->items));
    if(!ss->items) return NULL;
    ss->size=ss->size*2;
  }
  ss->top++;
  ss->items[ss->top]=c;

}

void destroy(stack* ss){
  free(ss->items);
  free(ss);
}

char pop(stack* ss){
  char c=ss->items[ss->top];
  ss->top--;
  return c;
}

int empty(stack* ss){
  return ss->top==-1;
}

int main(int argc,char** argv){
  stack* ss=make_stack(3);
  int c;
  while((c=getchar())!=EOF){
    push(ss,c);
  }

  destroy(ss);
}
