class LogicGate:
    def __init__(self, n):
        self.label = n
        self.output = None

    def getLabel(self):
        return self.label

    def getOutput(self):
        self.output = self.performLogic()
        return self.output


class BinaryGate(LogicGate):
    def __init__(self, n):
        LogicGate.__init__(self, n)
        self.pA = None
        self.pB = None

    def setNextPin(self, source):
        if self.pA is None:
            self.pA = source
        else:
            if self.pB is None:
                self.pB = source
            else:
                raise RuntimeError("Error: NO EMPTY PINS")

    def pinA(self):
        if self.pA is None:
            return int(
                input("Enter input for pin A for gate " + self.getLabel() +
                      "-->"))
        else:
            return self.pA.getFrom().getOutput()

    def pinB(self):
        if self.pB is None:
            return int(
                input("Enter input for pin B for gate " + self.getLabel() +
                      "-->"))
        else:
            return self.pB.getFrom().getOutput()


class UnaryGate(LogicGate):
    def __init__(self, n):
        LogicGate.__init__(self, n)
        self.p = None

    def setNextPin(self, source):
        if self.p is None:
            self.p = source
        else:
            raise RuntimeError("Error: NO EMPTY PINS")

    def pin(self):
        if self.p is None:
            return int(
                input("Enter input for pin of gate" + self.getLabel() + "-->"))
        else:
            return self.p.getFrom().getOutput()


class AndGate(BinaryGate):
    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performLogic(self):
        a = self.pinA()
        b = self.pinB()
        if a == 1 and b == 1:
            return 1
        else:
            return 0


class NotGate(UnaryGate):
    def __init__(self, n):
        UnaryGate.__init__(self, n)

    def performLogic(self):
        p = self.pin()
        if p == 1:
            return 0
        else:
            return 1


class OrGate(BinaryGate):
    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performLogic(self):
        a = self.pinA()
        b = self.pinB()
        if a == 1 or b == 1:
            return 1
        else:
            return 0


class Connector:
    def __init__(self, fgate, tgate):
        self.fromgate = fgate
        self.togate = tgate

        tgate.setNextPin(self)

    def getFrom(self):
        return self.fromgate

    def getTo(self):
        return self.togate


class NandGate(BinaryGate):
    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performLogic(self):
        and_g = AndGate(self.label + "_and")
        not_g = NotGate(self.label + "_not")
        Connector(and_g, not_g)
        return not_g.performLogic()


class XorGate(BinaryGate):
    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performLogic(self):
        not_g = NotGate(self.label + " not")
        not_g2 = NotGate(self.label + " not2")
        and_g = AndGate(self.label + " and")
        and_g2 = AndGate(self.label + " and2")
        or_g = OrGate(self.label + " or")
        Connector(not_g, and_g)
        Connector(and_g, or_g)
        Connector(not_g2, and_g2)
        Connector(and_g2, or_g)


class NorGate(BinaryGate):
    def __init__(self, n):
        BinaryGate.__init__(self, n)

    def performLogic(self):
        or_g = OrGate(self.label + "_or")
        not_g = NotGate(self.label + "_not")
        c1 = Connector(or_g, not_g)
        return not_g.performLogic()


xor = XorGate("xor")
xor.getOutput()
