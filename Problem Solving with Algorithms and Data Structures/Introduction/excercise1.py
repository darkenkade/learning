def gcd(m, n):
    m, n = abs(m), abs(n)
    if n > m:
        m, n = n, m
    while m % n != 0:
        oldm = m
        oldn = n

        m = oldn
        n = oldm % oldn
    return n


def fix_signs(num, den):
    if num < 0 and den < 0:
        num, den = -num, -den
    if num < 0:
        num, den = -num, -den
    return [num, den]


class Fraction:
    def __init__(self, top, bottom):
        if top == 0:
            self.num = 0
            self.den = 0
        elif bottom == 0:
            raise ValueError("0 in denominator")
        elif isinstance(top, int) and isinstance(bottom, int):
            n = gcd(top, bottom)
            top, bottom = fix_signs(top, bottom)
            self.num = top // n
            self.den = bottom // n
        else:
            raise TypeError("Num and den should be integers")

    def __str__(self):
        return str(self.num) + "/" + str(self.den)

    def __repr__(self):
        return (f'{self.__class__.__name__}(' f'{self.num!r}, {self.den!r})')

    def __radd__(self, number):
        if isinstance(number, int):
            return Fraction(self.num + number * self.den, self.den)
        else:
            raise TypeError("Expected integer")

    def __add__(self, other):
        if isinstance(other, int):
            return other + self
        return Fraction(self.num * other.den + self.den * other.num,
                        self.den * other.den)

    def __sub__(self, other):
        f = Fraction(self.num * other.den - self.den * other.num,
                     self.den * other.den)
        return f

    def __mul__(self, other):
        num = self.num * other.num
        den = self.den * other.den
        num, den = fix_signs(num, den)
        return Fraction(num, den)

    def __truediv__(self, other):
        num = self.num * other.den
        den = self.den * other.num
        num, den = fix_signs(num, den)
        return Fraction(num, den)

    def __eq__(self, other):
        return self.num == other.num and self.den == other.den

    def __ne__(self, other):
        return not self == other

    def __gt__(self, other):
        num1, num2 = self.same_den(other)
        return num1 > num2

    def __ge__(self, other):
        return self > other or self == other

    def __iadd__(self, other):
        print("__iadd__")
        f = self + other
        self.num = f.num
        self.den = f.den
        return self

    def __le__(self, other):
        return self < other or self == other

    def __lt__(self, other):
        num1, num2 = self.same_den(other)
        return num1 < num2

    def same_den(self, other):
        den = self.den * other.den
        return [den * self.num, den * other.num]


f = Fraction(1, 3)

print(f > Fraction(-14, 23))
print(Fraction(10, 13) + Fraction(-10, -13))
print(4 + Fraction(1, 2))
print(Fraction(1, 2) + 4)
f += Fraction(2, 3)
str(f)
repr(f)
